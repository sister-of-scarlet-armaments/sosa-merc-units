# SoSA MERC Contractors
Made for Ace's Deathstrider mod.

A few of the various contractors SoSA employs, specifically in regards to the DooM multiverse.
Included are the files for Doomguy, Imp-Tan, and a HDest styled player class.

# Note
Each folder inside is to be loaded as its own thing, this is for those that dont want to load certain players all the time.
So do not just drag and drop this repository in once you've downloaded it.

Both the HDest Guy and DoomGuy require the Zenith by Icarus Innovations.
Imp-Tan requires the P90 addon by Outrageous Productions.

# Credits
Imp-Tan:
- Mike12 for player sprites and sounds
- Mugshot was made by LossForWords or TG5 if I remember correctly, probably wrong though.

Doomguy:
- Mugshot by SomeGuyNamedMolinard
- Sounds from Zero X Diamonds original recordings
- Sprites modified from id's player sprites

HDest Guy:
- Mugshot and sprites by Craneo